@Author : Dilp udara wickramasinghe

There are 2 pakcages.
1)vn.lazada.pageobjects - This is for all the page objects
2)vn.lazada.tests - Test Scripts, class for config reader
3)resources.Config - Config file

PageObjects contains : 

LazadaHomePage
LazadaPageSignUp
LazadaProductInfomation
LazadaShoppingCart

automation_FrameWork contains : 

ConfigReader		    		- This user to read URL from the config file
ProductInfomationValidation 		- This related to 2nd Scnario mentioned in Test
RegisterNewaccountErrorValidation	-This related to 1st Scnario mentioned in test
SubTotalValidationOnCatrPoPup           -This related to 3rd scnario mentioned in test
TestBase				- This is set up calss for all test

Note: 
Used TestNg anotations 

Issues found : 

1)ProductInfomationValidation  - delivery days is not in cart page so cannot validate dilivery date. Therefore price and name are validated

2)SubTotalValidationOnCatrPoPup  -(3rd scnario) Shopping cart iframe cannot be identify using webdriver.



Test case Status : 

Scenario 01  : validate Error message
		Completed and passed


Scenario 02 : Validate product infomation
		Completed and passed.

Scenario 03 : validate subtotal price 
		completed.(iframe cannot be identified.However test is written for validate the subtotal price.This test cannot run fully due to iframe issue)





How to create Test Environemt : 
1)Install java(jdk) 
2)download and Set up eclipse
3)Setup webdriver(download webdriver java client)
4)configure eclips with webdriver
5)Open Lazada project from eclipse





 




package vn.lazada.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LazadaPageSignUp {
	private WebDriver driver;
	private WebElement element = null;
	
	public LazadaPageSignUp(WebDriver driver ) {
		   this.driver = driver;
		// TODO Auto-generated constructor stub
	}
	
	public void swichtoIframe()
	{
		driver.switchTo().frame(0);
	}
	
	public void enterName(String name)
	{
		element  =driver.findElement(By.id("RegistrationForm_first_name"));
		element.sendKeys(name);
	}
	
	public void enterEmail(String email)
	{
		element  =driver.findElement(By.id("RegistrationForm_email"));
		element.sendKeys(email);
	}
	public void enterPassword(String password)
	{
		element  =driver.findElement(By.id("RegistrationForm_password"));
		element.sendKeys(password);
	}
	public void enterBirthyear(String year)
	{
		element  =driver.findElement(By.id("RegistrationForm_year"));
		element.sendKeys(year);
	}
	public void enterBirthMonth(String month)
	{
		element  =driver.findElement(By.id("RegistrationForm_month"));
		element.sendKeys(month);
	}
	public void enterBirthDay(String day)
	{
		element  =driver.findElement(By.id("RegistrationForm_day"));
		element.sendKeys(day);
		
	}
	public void enterGender(String gender)
	{
		element  =driver.findElement(By.id("RegistrationForm_gender"));
		element.sendKeys(gender);
		
	}
	public void clickSubmit()
	{
		element  =driver.findElement(By.id("send"));
		element.click();
		
	}
	public String verifyErrormessage()
	{
		element  =driver.findElement(By.xpath(".//*[@id='popup-form-account-create']/fieldset/div/div[4]/div[2]/div/span"));
		return 	element.getText().trim();
		
	}
}

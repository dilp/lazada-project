package vn.lazada.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LazadaProductInfomation {
	
	private WebDriver driver;
	public LazadaProductInfomation(WebDriver driver) {
		  this.driver = driver;
		// TODO Auto-generated constructor stub
	}
	

	public static WebElement element = null;
	
	
	
	public void viewproductdetailsforfirsritem() // need to method name
	{
	
		
		element  =driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]"));
		element.click();
		
		
		
	}
	public String viewNameOftheprduct()
	{
		element  =driver.findElement(By.xpath(".//*[@id='prod_title']"));
		String	name =element.getText().trim();
		
		return name;
		
	}

	public int viewPriceOftheprduct() {
		element = driver.findElement(By.xpath(".//*[@id='special_price_box']"));
		String price = element.getText().trim();
		int priceLength;
		priceLength = price.length();
		// Removing last 3 string
		price = price.substring(0, priceLength - 3);
		return Integer.parseInt(price);

	}
	public String viewdiliveryDateOftheprduct(String diliveryDate)
	{
		element  =driver.findElement(By.xpath(".//*[@id='prod_content_wrapper']/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/span[2]"));
		diliveryDate=element.getText().trim();
		
		return diliveryDate;
		
	}
	public void addtoShoppingCart()
	{
		element  =driver.findElement(By.xpath(".//*[@id='AddToCart']"));
		element.click();
	
	}
	public void closedetailspanel()
	{
		element  =driver.findElement(By.xpath(".//*[@id='cartContinueShopping']/span"));
		element.click();
	}
	//add 1st product to cart
	public void addfirtstmic() throws Exception
	
	{
		try{
		element  =driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]"));
		element.click();
		element  =driver.findElement(By.xpath(".//*[@id='AddToCart']"));
		element.click();
		//close and continue shopping 
		element  =driver.findElement(By.xpath(".//*[@id='cartContinueShopping']"));
		element.click();
		}catch(Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}
	
	//add 2nd product
	public void addSecondHeadSet() throws Exception
	{
		try
		{
		element  =driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div/div[2]/a[2]"));
		element.click();
		element  =driver.findElement(By.xpath(".//*[@id='AddToCart']"));
		element.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//close and continue shopping 
		element  =driver.findElement(By.xpath(".//*[@id='cartContinueShopping']"));
		element.click();
		}catch(Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}
	
}

package vn.lazada.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class LazadaHomePage   {
	
	private WebDriver driver;
	
	public LazadaHomePage(WebDriver driver) {
	       this.driver = driver;
		// TODO Auto-generated constructor stub
	}
	
	//public  WebDriver driver;
	public static WebElement element = null;


	//Method - to Navigate Sign up page
	public LazadaPageSignUp navigatToSignUp() throws Exception {
		try {

			element = driver.findElement(By
					.xpath(".//span[contains(text(),'Signup')]"));
			element.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return new LazadaPageSignUp(driver);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	//Method - headphone section
	public LazadaProductInfomation naviagateToproductInfomation() throws Exception
	{
		try{
			element  =driver.findElement(By.xpath("//a[contains(text(),'headphone')]"));
			element.click();
		return new LazadaProductInfomation(driver);
		}catch(Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}
	
	
	
	//Navigate to the cart for 1st item
	public LazadaShoppingCart navigateToShoppongcart() throws Exception
	{
		try{
			element  =driver.findElement(By.xpath("//div[contains(@class,'header__cart headCart')]"));
			element.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return new LazadaShoppingCart(driver);
	}catch(Exception e)
	{
		throw new Exception(e.getMessage());
	}

}

	
}
package vn.lazada.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LazadaShoppingCart {
	private WebDriver driver;
	public LazadaShoppingCart(WebDriver driver) {
		  this.driver = driver;
		// TODO Auto-generated constructor stub
	}
	public LazadaShoppingCart() {
		// TODO Auto-generated constructor stub
	}
	public static WebElement element = null;
	
		
	
	public String viewdiliveryDateoftheproductoncart( )
	{
		element  =driver.findElement(By.xpath(".//*[@id='prod_content_wrapper']/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/span[2]"));
		String diliveryDate=element.getText().trim();
		
		return diliveryDate;
		
	}
	public String viewNameoftheproductoncart( )
	{
		element  =driver.findElement(By.xpath(".//*[@id='cart-items-list-form']/div[2]/table/tbody/tr/td[2]/div[1]"));
		String name =element.getText().trim();
		
		return name;		
	}

	public int viewpriceoftheproductoncart() throws Exception {
		try {
			element = driver
					.findElement(By
							.xpath(".//*[@id='cart-items-list-form']/div[2]/table/tbody/tr/td[3]/span[1]"));
			String price = element.getText().trim();
			int priceLength;
			priceLength = price.length();
			// Removing last 3 string
			price = price.substring(0, priceLength - 3);
			return Integer.parseInt(price);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void closeCartpannel()
	{
		element  =driver.findElement(By.xpath("html/body/div[8]/div/a"));
		element.click();
	}
	public String calculateSubTotal() throws Exception
	{
		String subTotal = null;
		try{			
		//driver.switchTo().frame("nyroModalLink");
		
		//element  =driver.findElement(By.xpath("//*[@id='cart-items-list-form']/div[@id='subtotal']/table/tbody/tr[1]/td[3]/div[1]"));
			element  =driver.findElement(By.xpath(".//*[@id='cartContinueShopping']/span"));
		subTotal =element.getText().trim();
	
		}
		 catch(Exception e){
			System.out.println(e.getMessage()); 
		 }
		
		return subTotal;
		
	}
	
	public void changeQTY(String qty)
	{
		element  =driver.findElement(By.xpath(".//*[@id='qty_ST011ELAGCDQVNAMZ-327404']"));
		element.sendKeys(qty);
		
	}
	
	
	
	
	
}

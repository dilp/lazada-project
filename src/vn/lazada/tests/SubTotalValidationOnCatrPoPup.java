package vn.lazada.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import vn.lazada.pageobjects.LazadaHomePage;
import vn.lazada.pageobjects.LazadaProductInfomation;
import vn.lazada.pageobjects.LazadaShoppingCart;

public class SubTotalValidationOnCatrPoPup  extends TestBase{
	
	String subtoatal1;
	String subtotal2;
	
	@Test
	public void validateSubtotal() throws Exception
	{
		try
		{
		LazadaHomePage homePage = new LazadaHomePage(driver);
		LazadaProductInfomation productInfo =homePage.naviagateToproductInfomation();
		productInfo.addfirtstmic();
		homePage.naviagateToproductInfomation();
		productInfo.addSecondHeadSet();
		homePage.navigateToShoppongcart();
		LazadaShoppingCart shoppingCart = new LazadaShoppingCart();
		
		//sub total before change
		subtoatal1 =shoppingCart.calculateSubTotal();
		shoppingCart.changeQTY("4");
		subtotal2 =shoppingCart.calculateSubTotal();
		Assert.assertNotSame(subtoatal1, subtotal2);
		
		
		}catch(Exception e)
		{
			throw new Exception(e.getMessage());
		}
		
		
	
		
		
	}

}

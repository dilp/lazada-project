package vn.lazada.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestBase {

	public static WebDriver driver;
	private String url;

	@BeforeClass
	public WebDriver setupClass() {
		ConfigReader objcon = new ConfigReader();
		url = objcon.readConfig("baseUrl");
		driver = new FirefoxDriver();

		driver.get(url);
		driver.manage().window().maximize();

		return driver;
	}

	

	@AfterClass
	public void tearDownLadza() {
		driver.quit();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

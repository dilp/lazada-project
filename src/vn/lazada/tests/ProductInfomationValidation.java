package vn.lazada.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import vn.lazada.pageobjects.LazadaHomePage;
import vn.lazada.pageobjects.LazadaProductInfomation;
import vn.lazada.pageobjects.LazadaShoppingCart;

public class ProductInfomationValidation extends TestBase {
	String ProductName;
	int ProductPrice;
	String ProductNameOnCart;
	int ProductPriceOnCart;

	@Test
	public void productdetailsvalidationwithCart() throws Exception {
		try {
			LazadaHomePage homePage = new LazadaHomePage(driver);
			LazadaProductInfomation productInfo = homePage
					.naviagateToproductInfomation();

			homePage.naviagateToproductInfomation();
			productInfo.viewproductdetailsforfirsritem();

			ProductName = productInfo.viewNameOftheprduct();
			ProductPrice = productInfo.viewPriceOftheprduct();
			productInfo.addtoShoppingCart();
			productInfo.closedetailspanel();

			LazadaShoppingCart shoppingCart = homePage
					.navigateToShoppongcart();

			ProductNameOnCart = shoppingCart.viewNameoftheproductoncart();

			ProductPriceOnCart = shoppingCart.viewpriceoftheproductoncart();

			// Assertions -validate product page details and cart page details
			Assert.assertEquals(ProductName, ProductNameOnCart);
			Assert.assertEquals(ProductPrice, ProductPriceOnCart);

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}

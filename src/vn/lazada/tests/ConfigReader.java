package vn.lazada.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

	public String readConfig(String configName) {
		Properties prop = new Properties();
		FileInputStream input = null;
		String configValue = null;

		try {
			String path = new File("").getAbsolutePath();
			path = path + "\\src\\resources\\Config\\config.properties";
			input = new FileInputStream(path);

			prop.load(input);

			// get the property value and print it out
			configValue = prop.getProperty(configName);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return configValue;

	}

}

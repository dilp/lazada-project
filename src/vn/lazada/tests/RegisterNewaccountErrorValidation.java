package vn.lazada.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import vn.lazada.pageobjects.LazadaHomePage;
import vn.lazada.pageobjects.LazadaPageSignUp;

public class RegisterNewaccountErrorValidation extends TestBase {

	@Test
	public void errorValaidation() throws Exception {

		String name = "dilp";
		String email = "dilpudara@gmail.com";
		String password = "test123456";
		String year = "1987";
		String month = "October";
		String day = "10";
		String gender = "male";
		String errorMessage = "Password must contain at least one number";

		// try
		// {//
		LazadaHomePage homePage = new LazadaHomePage(driver);
		LazadaPageSignUp signUpPage = homePage.navigatToSignUp();

		// signUpPage.swichtoIframe();
		signUpPage.enterName(name);
		signUpPage.enterEmail(email);
		signUpPage.enterPassword(password);
		signUpPage.enterBirthyear(year);
		signUpPage.enterBirthMonth(month);
		signUpPage.enterBirthDay(day);
		signUpPage.enterGender(gender);
		signUpPage.clickSubmit();
		Assert.assertEquals(signUpPage.verifyErrormessage(), errorMessage);
		System.out.print(signUpPage.verifyErrormessage());

		// }catch(Exception e)
		// {
		// throw new Exception(e.getLocalizedMessage());
		// }
		// }

	}
}